package com.getsoftware.githubusers.users.domain.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.getsoftware.githubusers.users.infrastructure.client.GithubUserApiClientHandler;
import com.getsoftware.githubusers.users.infrastructure.dto.GithubUserResponseDto;
import com.getsoftware.githubusers.users.infrastructure.mapper.GithubUserResponseDtoMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GithubUserServiceTest {

    private static final String LOGIN = "login";
    private static final String GITHUB_API_RESPONSE = "githubApiResponse";

    @InjectMocks
    private GithubUserService githubUserService;
    @Mock
    private GithubUserRequestCounterService githubUserRequestCounterService;
    @Mock
    private GithubUserApiClientHandler githubUserApiClientHandler;
    @Mock
    private GithubUserResponseDtoMapper githubUserResponseDtoMapper;
    @Mock
    private ObjectMapper objectMapper;

    @Test
    void shouldPerformRequestCountingAndFetchingUserAndMapToGithubUser() throws JsonProcessingException {
        //given
        when(githubUserApiClientHandler.getRawGithubUserByLogin(LOGIN)).thenReturn(GITHUB_API_RESPONSE);
        when(objectMapper.readValue(GITHUB_API_RESPONSE, GithubUserResponseDto.class)).thenReturn(new GithubUserResponseDto());

        //when
        githubUserService.getGithubUserByLogin(LOGIN);

        //then
        verify(githubUserRequestCounterService).performRequestCounting(LOGIN);
        verify(githubUserApiClientHandler).getRawGithubUserByLogin(LOGIN);
        verify(objectMapper).readValue(GITHUB_API_RESPONSE, GithubUserResponseDto.class);
        verify(githubUserResponseDtoMapper).map(any(GithubUserResponseDto.class));
    }
}

package com.getsoftware.githubusers.users.domain.service;

import com.getsoftware.githubusers.users.domain.model.GithubUserRequestCounter;
import com.getsoftware.githubusers.users.domain.repository.GithubUserRequestCounterRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GithubUserRequestCounterServiceTest {

    private static final String LOGIN = "login";

    @InjectMocks
    private GithubUserRequestCounterService githubUserRequestCounterService;
    @Mock
    private GithubUserRequestCounterRepository githubUserRequestCounterRepository;

    @Test
    void shouldPerformRequestCountingForNotExistingGitlabUserRequestCounter() {
        //given
        GithubUserRequestCounter githubUserRequestCounter = GithubUserRequestCounter.builder()
                .login(LOGIN)
                .build();

        when(githubUserRequestCounterRepository.getOneByLogin(LOGIN)).thenReturn(Optional.empty());

        //when
        githubUserRequestCounterService.performRequestCounting(LOGIN);

        //then
        verify(githubUserRequestCounterRepository).save(githubUserRequestCounter);
    }
}

package com.getsoftware.githubusers.users.infrastructure.mapper;

import com.getsoftware.githubusers.users.domain.model.GithubUser;
import com.getsoftware.githubusers.users.infrastructure.dto.GithubUserResponseDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class GithubUserResponseDtoMapperTest {

    private static final long ID = 1L;
    private static final String LOGIN = "login";
    private static final String NAME = "name";
    private static final String TYPE = "type";
    private static final String AVATAR_URL = "avatarUrl";
    private static final LocalDateTime CREATED_AT = LocalDateTime.MAX;
    private static final long NUMBER_OF_FOLLOWERS = 5L;
    private static final long NUMBER_OF_PUBLIC_REPORTS = 3L;

    @InjectMocks
    private GithubUserResponseDtoMapperImpl githubUserRequestDtoMapper;

    private GithubUserResponseDto githubUserResponseDto;

    @BeforeEach
    void init() {
        githubUserResponseDto =
                GithubUserResponseDto.builder()
                        .id(ID)
                        .login(LOGIN)
                        .name(NAME)
                        .type(TYPE)
                        .avatarUrl(AVATAR_URL)
                        .createdAt(CREATED_AT)
                        .followers(NUMBER_OF_FOLLOWERS)
                        .publicReports(NUMBER_OF_PUBLIC_REPORTS)
                        .build();
    }

    @Test
    void shouldMapToGithubUser() {
        //when
        GithubUser githubUser = githubUserRequestDtoMapper.map(githubUserResponseDto);

        //then
        assertThat(githubUser.getId()).isEqualTo(ID);
        assertThat(githubUser.getLogin()).isEqualTo(LOGIN);
        assertThat(githubUser.getName()).isEqualTo(NAME);
        assertThat(githubUser.getType()).isEqualTo(TYPE);
        assertThat(githubUser.getAvatarUrl()).isEqualTo(AVATAR_URL);
        assertThat(githubUser.getCreatedAt()).isEqualTo(CREATED_AT);
        assertThat(githubUser.getFollowers()).isEqualTo(NUMBER_OF_FOLLOWERS);
        assertThat(githubUser.getPublicReports()).isEqualTo(NUMBER_OF_PUBLIC_REPORTS);
    }
}

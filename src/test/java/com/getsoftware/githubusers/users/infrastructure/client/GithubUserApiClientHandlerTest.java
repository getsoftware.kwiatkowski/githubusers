package com.getsoftware.githubusers.users.infrastructure.client;

import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryRegistry;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.io.IOException;

import static com.getsoftware.githubusers.users.infrastructure.constants.Constants.GITHUB_CLIENT_CIRCUIT_BREAKER_CONFIG_NAME;
import static com.getsoftware.githubusers.users.infrastructure.constants.Constants.GITHUB_CLIENT_RETRY_CONFIG_NAME;
import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
class GithubUserApiClientHandlerTest {

    private static final String LOGIN = "githubLogin";
    private static final String BODY = """
            {
            "login": "octocat",
            "id": 583231,
            "avatar_url": "https://avatars.githubusercontent.com/u/583231?v=4",
            "type": "User",
            "name": "The Octocat",
            "public_repos": 8,
            "followers": 11506,
            "created_at": "2011-01-25T18:44:36Z"
            }
            """;
    private static final String ERROR = "ERROR";

    @Autowired
    private WebApplicationContext webApplicationContext;
    @Autowired
    private CircuitBreakerRegistry circuitBreakerRegistry;
    @Autowired
    private RetryRegistry retryRegistry;

    private MockMvc mockMvc;
    private MockWebServer mockWebServer;
    private CircuitBreaker circuitBreaker;
    private Retry retry;

    @BeforeEach
    void init() throws IOException {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(this.webApplicationContext).build();

        this.circuitBreaker = circuitBreakerRegistry.circuitBreaker(GITHUB_CLIENT_CIRCUIT_BREAKER_CONFIG_NAME);
        this.retry = retryRegistry.retry(GITHUB_CLIENT_RETRY_CONFIG_NAME);

        this.mockWebServer = new MockWebServer();
        this.mockWebServer.start(12345);
    }

    @AfterEach
    void tearDown() throws IOException {
        this.mockWebServer.shutdown();
    }

    @Test
    void shouldRetryThreeTimesAndSwitchCircuitBreakerToOpenState() throws Exception {
        //given
        mockWebServer.enqueue(new MockResponse().setBody(ERROR).setResponseCode(500));
        mockWebServer.enqueue(new MockResponse().setBody(ERROR).setResponseCode(500));
        mockWebServer.enqueue(new MockResponse().setBody(ERROR).setResponseCode(500));

        //when
        mockMvc.perform(get("/users/{login}", LOGIN)
                .contentType(MediaType.APPLICATION_JSON));

        //then
        assertThat(mockWebServer.getRequestCount()).isEqualTo(3);
        assertThat(circuitBreaker.getState()).isEqualTo(CircuitBreaker.State.OPEN);
    }

    @Test
    void shouldRetryTwoTimesAndFetchSuccessResponse() throws Exception {
        //given
        mockWebServer.enqueue(new MockResponse().setBody(ERROR).setResponseCode(500));
        mockWebServer.enqueue(new MockResponse().setBody(BODY).setResponseCode(200));

        //when
        ResultActions result =
                mockMvc.perform(get("/users/{login}", LOGIN)
                        .contentType(MediaType.APPLICATION_JSON_VALUE));

        //then
        result.andExpect(jsonPath("$.id").value(583231));
        result.andExpect(jsonPath("$.login").value("octocat"));
        result.andExpect(jsonPath("$.name").value("The Octocat"));
        result.andExpect(jsonPath("$.type").value("User"));
        result.andExpect(jsonPath("$.avatarUrl").value("https://avatars.githubusercontent.com/u/583231?v=4"));
        result.andExpect(jsonPath("$.createdAt").value("2011-01-25T18:44:36"));
        result.andExpect(jsonPath("$.calculations").value(0.0052146707));

        assertThat(mockWebServer.getRequestCount()).isEqualTo(2);
        assertThat(circuitBreaker.getState()).isEqualTo(CircuitBreaker.State.CLOSED);
    }
}

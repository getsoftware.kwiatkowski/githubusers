package com.getsoftware.githubusers.users.infrastructure.service;

import com.getsoftware.githubusers.users.application.v1.service.GithubUserDtoMapperService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(MockitoExtension.class)
class GithubUserDtoMapperServiceTest {

    @InjectMocks
    private GithubUserDtoMapperService githubUserResponseDtoMapperService;

    @Test
    void shouldPerformCalculation() {
        //given
        Long followers = 11053L;
        Long publicReports = 8L;

        //when
        Float result = githubUserResponseDtoMapperService.getCalculations(followers, publicReports);

        assertThat(result).isEqualTo(0.0054283906f);
    }
}

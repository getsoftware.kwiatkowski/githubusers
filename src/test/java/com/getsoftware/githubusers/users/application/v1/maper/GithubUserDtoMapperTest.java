package com.getsoftware.githubusers.users.application.v1.maper;

import com.getsoftware.githubusers.users.application.v1.dto.GithubUserDto;
import com.getsoftware.githubusers.users.application.v1.service.GithubUserDtoMapperService;
import com.getsoftware.githubusers.users.domain.model.GithubUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class GithubUserDtoMapperTest {

    private static final long ID = 1L;
    private static final String LOGIN = "login";
    private static final String NAME = "name";
    private static final String TYPE = "type";
    private static final String AVATAR_URL = "avatarUrl";
    private static final LocalDateTime CREATED_AT = LocalDateTime.MAX;
    private static final long NUMBER_OF_FOLLOWERS = 5L;
    private static final long NUMBER_OF_PUBLIC_REPORTS = 3L;
    private static final Float CALCULATION_RESULT = 3.555f;

    @InjectMocks
    private GithubUserDtoMapperImpl githubUserDtoMapper;
    @Mock
    private GithubUserDtoMapperService githubUserDtoMapperService;

    private GithubUser githubUser;

    @BeforeEach
    void init()  {
        githubUser = GithubUser.builder()
                .id(ID)
                .login(LOGIN)
                .name(NAME)
                .type(TYPE)
                .avatarUrl(AVATAR_URL)
                .createdAt(CREATED_AT)
                .followers(NUMBER_OF_FOLLOWERS)
                .publicReports(NUMBER_OF_PUBLIC_REPORTS)
                .build();
    }

    @Test
    void shouldMapToGithubUserDto() {
        //given
        when(githubUserDtoMapperService.getCalculations(NUMBER_OF_FOLLOWERS, NUMBER_OF_PUBLIC_REPORTS)).thenReturn(CALCULATION_RESULT);

        //when
        GithubUserDto githubUserDto = githubUserDtoMapper.map(githubUser);

        //then
        assertThat(githubUserDto.getId()).isEqualTo(ID);
        assertThat(githubUserDto.getLogin()).isEqualTo(LOGIN);
        assertThat(githubUserDto.getName()).isEqualTo(NAME);
        assertThat(githubUserDto.getType()).isEqualTo(TYPE);
        assertThat(githubUserDto.getAvatarUrl()).isEqualTo(AVATAR_URL);
        assertThat(githubUserDto.getCreatedAt()).isEqualTo(CREATED_AT);
        assertThat(githubUserDto.getCalculations()).isEqualTo(CALCULATION_RESULT);
    }
}

package com.getsoftware.githubusers.common.domain.model;

import com.getsoftware.githubusers.common.domain.converter.UUIDConverter;
import lombok.*;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;
import java.util.UUID;

@SuperBuilder
@MappedSuperclass
@NoArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
public abstract class BaseIdentityEntity extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hibernate_sequence")
    @SequenceGenerator(name = "hibernate_sequence", sequenceName = "hibernate_sequence", allocationSize = 1)
    @Column(nullable = false, updatable = false, unique = true)
    @Getter
    private Long id;

    @EqualsAndHashCode.Include
    @Builder.Default
    @Convert(converter = UUIDConverter.class)
    @Column(nullable = false, updatable = false, unique = true)
    @Getter
    private String uuid = UUID.randomUUID().toString();
}

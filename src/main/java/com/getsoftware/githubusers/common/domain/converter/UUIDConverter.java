package com.getsoftware.githubusers.common.domain.converter;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import javax.xml.bind.DatatypeConverter;
import java.util.UUID;

@Converter
public class UUIDConverter implements AttributeConverter<String, String> {

    @Override
    public String convertToDatabaseColumn(final String attribute) {
        if (attribute.length() == 36) {
            UUID uuid = UUID.fromString(attribute);
            byte[] uuidArr = asByteArray(uuid);

            return DatatypeConverter.printBase64Binary(uuidArr).split("=")[0];
        }
        return attribute;
    }

    @Override
    public String convertToEntityAttribute(final String dbData) {
        return dbData;
    }

    private static byte[] asByteArray(final UUID uuid) {

        long msb = uuid.getMostSignificantBits();
        long lsb = uuid.getLeastSignificantBits();
        byte[] buffer = new byte[16];

        for (int i = 0; i < 8; i++) {
            buffer[i] = (byte) (msb >>> 8 * (7 - i));
        }
        for (int i = 8; i < 16; i++) {
            buffer[i] = (byte) (lsb >>> 8 * (7 - i));
        }

        return buffer;
    }
}

package com.getsoftware.githubusers.users.infrastructure.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

public class GithubUserApiResponseStatusException extends ResponseStatusException {

    private final HttpStatus status;
    private final String message;

    public GithubUserApiResponseStatusException(HttpStatus statusCode, String message) {
        super(statusCode, message);
        this.status = HttpStatus.resolve(statusCode.value());
        this.message = message;
    }

    public HttpStatus getStatus() {
        return status;
    }

    @Override
    public String getMessage() {
        return message;
    }
}

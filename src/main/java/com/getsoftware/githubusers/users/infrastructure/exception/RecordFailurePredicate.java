package com.getsoftware.githubusers.users.infrastructure.exception;

import io.netty.handler.timeout.TimeoutException;
import org.springframework.web.reactive.function.client.WebClientException;

import java.util.function.Predicate;

public class RecordFailurePredicate implements Predicate<Throwable> {
    @Override
    public boolean test(Throwable e) {
        return recordFailures(e);
    }

    private boolean recordFailures(Throwable throwable) {
        return
                (throwable instanceof GithubUserApiResponseStatusException ex && ex.getStatus().is5xxServerError()) ||
                        throwable instanceof TimeoutException ||
                        throwable instanceof WebClientException;
    }
}

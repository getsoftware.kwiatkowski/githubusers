package com.getsoftware.githubusers.users.infrastructure.client;

import com.getsoftware.githubusers.users.infrastructure.exception.GithubUserApiResponseStatusException;
import com.getsoftware.githubusers.users.infrastructure.exception.UserNotFoundException;
import io.github.resilience4j.circuitbreaker.CallNotPermittedException;
import io.github.resilience4j.circuitbreaker.CircuitBreaker;
import io.github.resilience4j.circuitbreaker.CircuitBreakerRegistry;
import io.github.resilience4j.reactor.circuitbreaker.operator.CircuitBreakerOperator;
import io.github.resilience4j.reactor.retry.RetryOperator;
import io.github.resilience4j.retry.Retry;
import io.github.resilience4j.retry.RetryRegistry;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import reactor.core.publisher.Mono;

import static com.getsoftware.githubusers.users.infrastructure.constants.Constants.GITHUB_CLIENT_CIRCUIT_BREAKER_CONFIG_NAME;
import static com.getsoftware.githubusers.users.infrastructure.constants.Constants.GITHUB_CLIENT_RETRY_CONFIG_NAME;
import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;

@Slf4j
@Component
public class GithubUserApiClientHandler {

    private static final String URI_GET_USER_BY_LOGIN = "/users";
    private final GithubUserApiClient githubUserApiClient;
    private final CircuitBreaker circuitBreaker;
    private final Retry retry;

    public GithubUserApiClientHandler(GithubUserApiClient githubUserApiClient, CircuitBreakerRegistry circuitBreakerRegistry, RetryRegistry retryRegistry) {
        this.githubUserApiClient = githubUserApiClient;
        this.circuitBreaker = circuitBreakerRegistry.circuitBreaker(GITHUB_CLIENT_CIRCUIT_BREAKER_CONFIG_NAME);
        this.retry = retryRegistry.retry(GITHUB_CLIENT_RETRY_CONFIG_NAME);
    }

    public String getRawGithubUserByLogin(String login) {

        return githubUserApiClient.getGithubClient()
                .get()
                .uri(uriBuilder -> uriBuilder.path(URI_GET_USER_BY_LOGIN + "/{login}").build(login))
                .retrieve()
                .onStatus(HttpStatus::isError, clientResponse -> handleErrorResponse(clientResponse, login))
                .bodyToMono(String.class)
                .transformDeferred(CircuitBreakerOperator.of(circuitBreaker))
                .transformDeferred(RetryOperator.of(retry))
                .doOnError(CallNotPermittedException.class::isInstance, throwable -> {
                    log.error("Circuit Breaker is in [{}]... Providing fallback response without calling the API", circuitBreaker.getState());
                    throw new GithubUserApiResponseStatusException(SERVICE_UNAVAILABLE, "API service is unavailable");
                })
                .block();
    }

    private Mono<GithubUserApiResponseStatusException> handleErrorResponse(final ClientResponse clientResponse, String login) {

        HttpStatus responseStatus = clientResponse.statusCode();
        log.info("Handling error response: [{}]", responseStatus);

        if (HttpStatus.NOT_FOUND == responseStatus) {
            return Mono.error(new UserNotFoundException(String.format("User with login '%s' does not exist", login)));
        }

        return clientResponse
                .bodyToMono(String.class)
                .defaultIfEmpty("")
                .flatMap(res -> Mono.just(new GithubUserApiResponseStatusException(responseStatus, res)));
    }
}

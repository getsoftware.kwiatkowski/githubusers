package com.getsoftware.githubusers.users.infrastructure.mapper;

import com.getsoftware.githubusers.users.domain.model.GithubUser;
import com.getsoftware.githubusers.users.infrastructure.dto.GithubUserResponseDto;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface GithubUserResponseDtoMapper {

    GithubUser map(GithubUserResponseDto githubUserResponseDto);
}

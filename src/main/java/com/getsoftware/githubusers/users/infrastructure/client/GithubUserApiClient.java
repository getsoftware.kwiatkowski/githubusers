package com.getsoftware.githubusers.users.infrastructure.client;

import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

import static io.netty.channel.ChannelOption.CONNECT_TIMEOUT_MILLIS;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static org.springframework.http.HttpHeaders.ACCEPT;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static reactor.core.publisher.Mono.just;

@Slf4j
@Component
public class GithubUserApiClient {

    private final int connectTimeout;
    private final int readTimeout;
    private final int writeTimeout;

    @Getter
    private final WebClient githubClient;

    public GithubUserApiClient (
            @Value("${api.client.github.baseUrl}") String baseUrl,
            @Value("${api.client.github.connectTimeout}") int connectTimeout,
            @Value("${api.client.github.readTimeout}") int readTimeout,
            @Value("${api.client.github.writeTimeout}") int writeTimeout,
            @Value("${api.client.github.maxInMemorySize}") int maxInMemorySize) {

        this.connectTimeout = connectTimeout;
        this.readTimeout = readTimeout;
        this.writeTimeout = writeTimeout;

        this.githubClient =
                WebClient.builder()
                        .exchangeStrategies(
                                ExchangeStrategies.builder()
                                        .codecs(config -> config.defaultCodecs().maxInMemorySize(maxInMemorySize))
                                        .build()
                        )
                        .filter(logRequestDetails())
                        .filter(logResponseDetails())
                        .defaultHeader(CONTENT_TYPE, APPLICATION_JSON_VALUE)
                        .defaultHeader(ACCEPT, APPLICATION_JSON_VALUE)
                        .clientConnector(new ReactorClientHttpConnector(clientConnectorConfig()))
                        .baseUrl(baseUrl)
                        .build();
    }

    private ExchangeFilterFunction logRequestDetails() {

        return ExchangeFilterFunction.ofRequestProcessor(clientRequest -> {

            log.info("Sending [{}] request to URL [{}] with request headers [{}]",
                    clientRequest.method(), clientRequest.url(), clientRequest.headers()
            );

            return just(clientRequest);
        });
    }

    private ExchangeFilterFunction logResponseDetails() {

        return ExchangeFilterFunction.ofResponseProcessor(clientResponse ->
                clientResponse.bodyToMono(String.class).defaultIfEmpty("").flatMap(responseBody -> {

                    final ClientResponse orgClientResponse = clientResponse.mutate().body(responseBody).build();

                    log.info("Received response from API with body [{}] status [{}] with response headers [{}]",
                            responseBody, clientResponse.statusCode(), clientResponse.headers().asHttpHeaders().toSingleValueMap()
                    );

                    return just(orgClientResponse);
                })
        );
    }

    private HttpClient clientConnectorConfig() {
        return HttpClient.create().option(CONNECT_TIMEOUT_MILLIS, connectTimeout)
                .doOnConnected(conn -> {
                    conn.addHandlerLast(new ReadTimeoutHandler(readTimeout, MILLISECONDS));
                    conn.addHandlerLast(new WriteTimeoutHandler(writeTimeout, MILLISECONDS));
                });
    }
}

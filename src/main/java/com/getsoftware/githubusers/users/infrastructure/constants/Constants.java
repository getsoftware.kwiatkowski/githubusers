package com.getsoftware.githubusers.users.infrastructure.constants;

public class Constants {

    //githubServices
    public static final String GITHUB_CLIENT_RETRY_CONFIG_NAME = "GITHUB_CLIENT_RETRY_CONFIG_NAME";
    public static final String GITHUB_CLIENT_CIRCUIT_BREAKER_CONFIG_NAME = "GITHUB_CLIENT_CIRCUIT_BREAKER_CONFIG_NAME";
}

package com.getsoftware.githubusers.users.application.v1.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@NoArgsConstructor
@Setter
@Getter
public class GithubUserDto {

    private Long id;

    private String login;

    private String name;

    private String type;

    private String avatarUrl;

    private LocalDateTime createdAt;

    private Float calculations;
}

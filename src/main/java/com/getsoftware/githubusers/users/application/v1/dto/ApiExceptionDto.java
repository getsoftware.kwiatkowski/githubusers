package com.getsoftware.githubusers.users.application.v1.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Builder;
import lombok.Getter;

import java.time.LocalDateTime;

@Getter
@Builder
public class ApiExceptionDto {

    @Builder.Default
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private LocalDateTime timestamp = LocalDateTime.now();

    private String typeOfException;

    private String message;

    private int status;

    private String statusReason;
}

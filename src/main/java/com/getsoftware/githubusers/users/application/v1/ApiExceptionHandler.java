package com.getsoftware.githubusers.users.application.v1;

import com.getsoftware.githubusers.users.application.v1.dto.ApiExceptionDto;
import com.getsoftware.githubusers.users.infrastructure.exception.GithubUserApiResponseStatusException;
import com.getsoftware.githubusers.users.infrastructure.exception.UserNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.naming.ServiceUnavailableException;

@ControllerAdvice
@Slf4j
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler({UserNotFoundException.class})
    public ResponseEntity<ApiExceptionDto> handleNotFoundHttpStatus(Exception e) {

        return createResponse(HttpStatus.NOT_FOUND, e);
    }

    @ExceptionHandler({ServiceUnavailableException.class})
    public ResponseEntity<ApiExceptionDto> handleUnavailableHttpStatus(Exception e) {

        return createResponse(HttpStatus.SERVICE_UNAVAILABLE, e);
    }

    @ExceptionHandler({GithubUserApiResponseStatusException.class})
    public ResponseEntity<ApiExceptionDto> handleCustomResponseHttpStatus(Exception e) {

        return createResponse(HttpStatus.SERVICE_UNAVAILABLE, e);
    }
    private ResponseEntity<ApiExceptionDto> createResponse(HttpStatus status, Throwable e) {

        ApiExceptionDto apiExceptionDto = ApiExceptionDto.builder()
                .typeOfException(e.getClass().getSimpleName())
                .message(e.getMessage())
                .status(status.value())
                .statusReason(status.getReasonPhrase())
                .build();

        return createResponse(apiExceptionDto, e);
    }

    private ResponseEntity<ApiExceptionDto> createResponse(ApiExceptionDto apiExceptionDto, Throwable ex) {

        log.error("Exception: " + ex.getMessage());
        log.error("Converted into: {}", apiExceptionDto);

        return ResponseEntity
                .status(apiExceptionDto.getStatus())
                .contentType(MediaType.APPLICATION_JSON)
                .body(apiExceptionDto);
    }
}

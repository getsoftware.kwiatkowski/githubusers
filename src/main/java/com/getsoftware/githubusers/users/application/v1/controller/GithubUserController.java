package com.getsoftware.githubusers.users.application.v1.controller;

import com.getsoftware.githubusers.users.application.v1.dto.GithubUserDto;
import com.getsoftware.githubusers.users.application.v1.maper.GithubUserDtoMapper;
import com.getsoftware.githubusers.users.domain.service.GithubUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
public class GithubUserController {

    private final GithubUserService githubUserService;
    private final GithubUserDtoMapper githubUserDtoMapper;

    @GetMapping("{login}")
    public ResponseEntity<GithubUserDto> getGithubUserDtoByLogin(@PathVariable String login) {

        return ResponseEntity.ok(
                githubUserDtoMapper.map(githubUserService.getGithubUserByLogin(login))
        );
    }
}

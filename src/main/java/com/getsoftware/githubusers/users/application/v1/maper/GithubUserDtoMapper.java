package com.getsoftware.githubusers.users.application.v1.maper;

import com.getsoftware.githubusers.users.application.v1.dto.GithubUserDto;
import com.getsoftware.githubusers.users.application.v1.service.GithubUserDtoMapperService;
import com.getsoftware.githubusers.users.domain.model.GithubUser;
import lombok.RequiredArgsConstructor;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.Mappings;
import org.springframework.beans.factory.annotation.Autowired;

@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
@RequiredArgsConstructor
public abstract class GithubUserDtoMapper {

    public static final String CALCULATIONS = "calculations";

    public static final String GET_CALCULATIONS_FROM_GITHUB_USER_RESPONSE_DTO =
            "java(githubUserDtoMapperService.getCalculations(githubUser.getFollowers(),githubUser.getPublicReports()))";

    @Autowired
    GithubUserDtoMapperService githubUserDtoMapperService;

    @Mappings({
            @Mapping(target = CALCULATIONS,
                    expression = GET_CALCULATIONS_FROM_GITHUB_USER_RESPONSE_DTO
            )
    })
    public abstract GithubUserDto map(GithubUser githubUser);
}

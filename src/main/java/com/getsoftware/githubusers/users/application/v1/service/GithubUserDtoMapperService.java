package com.getsoftware.githubusers.users.application.v1.service;

import org.springframework.stereotype.Service;

@Service
public class GithubUserDtoMapperService {

    public Float getCalculations(Long followers, Long publicReports) {
        return 6.f/followers * (2 + publicReports);
    }
}

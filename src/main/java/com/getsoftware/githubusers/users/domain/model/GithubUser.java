package com.getsoftware.githubusers.users.domain.model;

import lombok.*;

import java.time.LocalDateTime;

@Setter
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class GithubUser {

    private Long id;

    private String login;

    private String name;

    private String type;

    private String avatarUrl;

    private LocalDateTime createdAt;

    private Long followers;

    private Long publicReports;
}

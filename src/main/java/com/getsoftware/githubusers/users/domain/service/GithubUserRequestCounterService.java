package com.getsoftware.githubusers.users.domain.service;

import com.getsoftware.githubusers.users.domain.model.GithubUserRequestCounter;
import com.getsoftware.githubusers.users.domain.repository.GithubUserRequestCounterRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@RequiredArgsConstructor
public class GithubUserRequestCounterService implements RequestCounter {

    private final GithubUserRequestCounterRepository githubUserRequestCounterRepository;

    @Override
    @Transactional
    public void performRequestCounting(String login) {

        githubUserRequestCounterRepository.getOneByLogin(login)
                .ifPresentOrElse(
                        GithubUserRequestCounter::incrementRequestCount,
                        () -> saveFirstGithubUserRequestCounter(login)
                );
    }

    private void saveFirstGithubUserRequestCounter(String login) {

        githubUserRequestCounterRepository.save(
                GithubUserRequestCounter.builder()
                .login(login)
                .build()
        );
    }
}

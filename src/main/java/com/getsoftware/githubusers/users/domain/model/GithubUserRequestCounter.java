package com.getsoftware.githubusers.users.domain.model;

import com.getsoftware.githubusers.common.domain.model.BaseIdentityEntity;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.*;

@Entity
@SuperBuilder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(onlyExplicitlyIncluded = true, callSuper = false)
@Table(name = "github_user_request_counter")
public class GithubUserRequestCounter extends BaseIdentityEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "hibernate_sequence")
    @SequenceGenerator(name = "hibernate_sequence", sequenceName = "hibernate_sequence", allocationSize = 1)
    @Column(nullable = false, unique = true, updatable = false)
    private Long id;

    @EqualsAndHashCode.Include
    @Column(name = "LOGIN", nullable = false, unique = true)
    private String login;

    @EqualsAndHashCode.Include
    @Column(name = "REQUEST_COUNT", nullable = false)
    @Builder.Default
    private Long requestCount = 1L;

    public void incrementRequestCount() {
        ++this.requestCount;
    }
}

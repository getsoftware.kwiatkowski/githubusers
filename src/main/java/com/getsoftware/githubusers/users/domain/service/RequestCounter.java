package com.getsoftware.githubusers.users.domain.service;

public interface RequestCounter {

    void performRequestCounting(String parameter);
}

package com.getsoftware.githubusers.users.domain.repository;

import com.getsoftware.githubusers.users.domain.model.GithubUserRequestCounter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface GithubUserRequestCounterRepository extends JpaRepository<GithubUserRequestCounter, String> {

    Optional<GithubUserRequestCounter> getOneByLogin(String login);
}

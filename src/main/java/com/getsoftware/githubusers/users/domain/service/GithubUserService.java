package com.getsoftware.githubusers.users.domain.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.getsoftware.githubusers.users.domain.model.GithubUser;
import com.getsoftware.githubusers.users.infrastructure.client.GithubUserApiClientHandler;
import com.getsoftware.githubusers.users.infrastructure.dto.GithubUserResponseDto;
import com.getsoftware.githubusers.users.infrastructure.mapper.GithubUserResponseDtoMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class GithubUserService {

    private final GithubUserRequestCounterService githubUserRequestCounterService;
    private final GithubUserApiClientHandler githubUserApiClientHandler;
    private final GithubUserResponseDtoMapper githubUserResponseDtoMapper;
    private final ObjectMapper objectMapper;

    @SneakyThrows
    public GithubUser getGithubUserByLogin(String login) {

        githubUserRequestCounterService.performRequestCounting(login);

        return githubUserResponseDtoMapper.map(
                objectMapper.readValue(
                        githubUserApiClientHandler.getRawGithubUserByLogin(login),
                        GithubUserResponseDto.class)
        );
    }
}

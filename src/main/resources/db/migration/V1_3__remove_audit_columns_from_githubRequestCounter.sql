begin transaction;

    alter table github_user_request_counter
        drop column created_date;

    alter table github_user_request_counter
        drop column modification_date;

commit;

begin transaction;

    create table github_user_request_counter (
         id bigint unique not null primary key,
         version bigint not null,
         uuid varchar(22) unique not null,
         created_date timestamp not null default now(),
         modification_date timestamp,
         LOGIN varchar(255) unique not null,
         REQUEST_COUNT bigint not null default 1
    );

commit;
